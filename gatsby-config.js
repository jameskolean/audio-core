require('dotenv').config({
  path: `.env.${process.env.NODE_ENV}`,
})

module.exports = {
  siteMetadata: {
    siteUrl: `https://audiocoredemo.gtsb.io/`,
  },
  plugins: [
    `gatsby-plugin-sitemap`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `markdown-locations`,
        path: `${__dirname}/src/markdown-locations`,
      },
    },
    `gatsby-transformer-remark`,
  ],
}
