import * as React from 'react'
import Layout from '../components/layout'

const About = ({ location }) => (
  <Layout location={location}>
    <h1>About</h1>
  </Layout>
)
export default About
