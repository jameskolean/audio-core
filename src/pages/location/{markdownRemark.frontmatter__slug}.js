import React from 'react'
import { graphql } from 'gatsby'
import Layout from '../../components/layout'

const Location = ({ location, data }) => {
  const { frontmatter } = data.markdownRemark
  return (
    <Layout location={location}>
      <h1>Location: {frontmatter.city}</h1>
      <p>This test is here to test preview with: {frontmatter.description}</p>
    </Layout>
  )
}
export default Location

export const pageQuery = graphql`
  query ($id: String!) {
    markdownRemark(id: { eq: $id }) {
      frontmatter {
        city
        description
      }
    }
  }
`
