import * as React from 'react'
import { Link, graphql } from 'gatsby'

import Layout from '../components/layout'
import { docLinkStyle } from '../util/styles'

const IndexPage = ({
  location,
  data: {
    allMarkdownRemark: { nodes },
  },
}) => {
  return (
    <Layout location={location}>
      {/* <main style={pageStyles}> */}
      <title>Home Page</title>
      <h1>Weclome to AudioCore</h1>
      <ul>
        {nodes.map((node, index) => (
          <li key={index} style={docLinkStyle}>
            <Link to={`/location${node.frontmatter.slug}`}>
              {node.frontmatter.city}
            </Link>
          </li>
        ))}
      </ul>
      {/* </main> */}
    </Layout>
  )
}

export default IndexPage

export const query = graphql`
  query IndexPageQuery {
    allMarkdownRemark {
      nodes {
        frontmatter {
          city
          slug
        }
      }
    }
  }
`
