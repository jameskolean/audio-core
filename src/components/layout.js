import React from 'react'
import { Link } from 'gatsby'
import { pageStyles } from '../util/styles'

const navStyle = {
  display: 'flex',
}
const navItemStyle = {
  paddingRight: '10px',
}
/**
 *
 * @param {*} location
 * @param {*} param1
 */
export default function Layout({ location, children }) {
  console.log('location', location)
  return (
    <>
      <header>
        <nav style={navStyle}>
          {location.pathname !== '/' && (
            <div>
              <Link style={navItemStyle} to='/'>
                Home
              </Link>
            </div>
          )}
          {location.pathname !== '/about' && (
            <div>
              <Link style={navItemStyle} to='/about'>
                About
              </Link>
            </div>
          )}
        </nav>
      </header>
      <main style={pageStyles}>{children}</main>
      <footer></footer>
    </>
  )
}
